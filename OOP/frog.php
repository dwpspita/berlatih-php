<?php

    require_once('animal.php');
    class Frog extends Animal{
        public $legs = 4;
        public $coldblooded = "no";
        public function Jump(){
            echo "Jump : Hop Hop";
        }
    }
?>