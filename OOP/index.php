<?php

    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');

    $Sheep = new Animal("shaun");
    echo "Name : " . $Sheep->name . "<br>";
    echo "Legs : " . $Sheep->legs . "<br>";
    echo "Cold Blooded : " . $Sheep->coldblooded . "<br>";
    echo "<br>";

    $kodok = new Frog("buduk");
    echo "Name : " . $kodok->name . "<br>";
    echo "Legs : " . $kodok->legs . "<br>";
    echo "Cold Blooded : " . $kodok->coldblooded . "<br>";
    echo $kodok -> Jump();
    echo "<br>";
    echo "<br>";

    $Sungokong = new Ape("kera sakti");
    echo "Name : " . $Sungokong->name . "<br>";
    echo "Legs : " . $Sungokong->legs . "<br>";
    echo "Cold Blooded : " . $Sungokong->coldblooded . "<br>";
    echo $Sungokong -> Yell();
    echo "<br>";
    echo "<br>";

?>